#include "FillColor.h"
#include <iostream>
using namespace std;

//Get color of a pixel
SDL_Color getPixelColor(Uint32 pixel_format, Uint32 pixel)
{
	SDL_PixelFormat* fmt = SDL_AllocFormat(pixel_format);

	Uint32 temp;
	Uint8 red, green, blue, alpha;

	/* Get Red component */
	temp = pixel & fmt->Rmask;  /* Isolate red component */
	temp = temp >> fmt->Rshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Rloss;  /* Expand to a full 8-bit number */
	red = (Uint8)temp;

	/* Get Green component */
	temp = pixel & fmt->Gmask;  /* Isolate green component */
	temp = temp >> fmt->Gshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Gloss;  /* Expand to a full 8-bit number */
	green = (Uint8)temp;

	/* Get Blue component */
	temp = pixel & fmt->Bmask;  /* Isolate blue component */
	temp = temp >> fmt->Bshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Bloss;  /* Expand to a full 8-bit number */
	blue = (Uint8)temp;

	/* Get Alpha component */
	temp = pixel & fmt->Amask;  /* Isolate alpha component */
	temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
	temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
	alpha = (Uint8)temp;

	SDL_Color color = { red, green, blue, alpha };
	return color;

}

//Get all pixels on the window
SDL_Surface* getPixels(SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {
	SDL_Surface* saveSurface = NULL;
	SDL_Surface* infoSurface = NULL;
	infoSurface = SDL_GetWindowSurface(SDLWindow);
	if (infoSurface == NULL) {
		std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
	}
	else {
		unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
		if (pixels == 0) {
			std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
			return NULL;
		}
		else {
			if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
				std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
				delete[] pixels;
				return NULL;
			}
			else {
				saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
			}
			delete[] pixels;
		}
	}
	return infoSurface;
}

//Compare two colors
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
	if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
		return true;
	return false;
}


SDL_Color GetColorAtPoint(SDL_Window *win, Vector2D currentPoint, Uint32 pixel_format, SDL_Renderer *ren)
{
	SDL_Surface* sface = getPixels(win, ren);
	Uint32 index = currentPoint.y * sface->w + currentPoint.x;
	Uint32 * pixel = (Uint32*)sface->pixels;
	Uint32 apixel = pixel[index];
	SDL_Color pixel_color = getPixelColor(pixel_format, apixel);
	pixel_color.a = 255;
	return pixel_color;
}


void BoundaryFill4(SDL_Window *win, Vector2D startPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	Vector2D *stack = new Vector2D[1000000];
	int numberofstack = 1;
	*(stack + numberofstack) = startPoint;

	while (true)
	{
		Vector2D currentPoint = *(stack + numberofstack);
		SDL_RenderDrawPoint(ren, currentPoint.x, currentPoint.y);

		Vector2D Left = Vector2D(currentPoint.x - 1, currentPoint.y);
		SDL_Color temp = GetColorAtPoint(win, Left, pixel_format, ren);
		if (!compareTwoColors(temp, boundaryColor) && !compareTwoColors(temp, fillColor))
		{
			numberofstack++;
			*(stack + numberofstack) = *(new Vector2D(currentPoint.x - 1, currentPoint.y));
			continue;
		}

		Vector2D Right = Vector2D(currentPoint.x + 1, currentPoint.y);
		 temp = GetColorAtPoint(win, Right, pixel_format, ren);
		if (!compareTwoColors(temp, boundaryColor) && !compareTwoColors(temp, fillColor))
		{
			numberofstack++;
			*(stack + numberofstack) = *(new Vector2D(currentPoint.x + 1, currentPoint.y));
			continue;
		}

		Vector2D Up = Vector2D(currentPoint.x , currentPoint.y -1 );
		 temp = GetColorAtPoint(win, Up, pixel_format, ren);
		if (!compareTwoColors(temp, boundaryColor) && !compareTwoColors(temp, fillColor))
		{
			numberofstack++;
			*(stack + numberofstack) = *(new Vector2D(currentPoint.x , currentPoint.y -1));
			continue;
		}

		Vector2D Down = Vector2D(currentPoint.x , currentPoint.y+1);
		 temp = GetColorAtPoint(win, Down, pixel_format, ren);
		if (!compareTwoColors(temp, boundaryColor) && !compareTwoColors(temp, fillColor))
		{
			numberofstack++;
			*(stack + numberofstack) = *(new Vector2D(currentPoint.x , currentPoint.y+1));
			continue;
		}

		numberofstack--;
		if (numberofstack == 0)
			break;
	}
	delete stack ;
}

//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int maxIn3(int a, int b, int c)
{
	if (a >= b && a >=c) {
		return a;
	}
	if ( b >=a && b >= c) {
		return b;
	}
	if (c>=a && c >=b) {
		return c;
	}
}

int minIn3(int a, int b, int c)
{
	if (a <= b && a <= c) {
		return a;
	}
	if (b <= a && b <= c) {
		return b;
	}
	if (c <= a && c <= b) {
		return c;
	}
}

void swap(Vector2D &a, Vector2D &b)
{
	Vector2D temp = a;
	a = b;
	b = temp;
}

void ascendingSort(Vector2D &v1, Vector2D &v2, Vector2D &v3)
{
	if (v1.y >= v2.y ) {
		swap(v1, v2);	
	}
	if (v1.y >= v3.y) {
		swap(v1, v3);
	}
	if (v2.y >= v3.y) {
		swap(v2, v3);
	}
}

/*
	1		2		3
	_________________

*/
void TriangleFill1(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	int xmin = minIn3(v1.x,v2.x,v3.x);
	int ymin = minIn3(v1.y,v2.y,v3.y);
	int xmax = maxIn3(v1.x, v2.x, v3.x);
	int ymax = maxIn3(v1.y,v2.y,v3.y);

	SDL_RenderDrawLine(ren, xmin,ymin,xmax,ymax);
}

/*		3
		/\
	   /  \
	  /    \
	 ________
	1		2
*/	
void TriangleFill2(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float d1 = (float)(v3.x - v1.x) / (v3.y - v1.y);
	float d3 = (float)(v3.x - v2.x) / (v3.y - v2.y);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float xLeft = v1.x;
	float xRight = v2.x;
	for (int y = v1.y; y <= v3.y; y++)
	{
		xLeft += d1;
		xRight += d3;
		DDA_Line(int(xLeft + 0.5), y, int(xRight + 0.5), y, ren);
	}
}



/*
  1	________2
	\	   /
	 \    /
	  \  /
	   \/3
*/
void TriangleFill3(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float d1 = (float)(v2.x - v1.x) / (v2.y - v1.y);
	float d2 = (float)(v3.x - v2.x) / (v3.y - v2.y);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float xLeft = v1.x;
	float xRight = v1.x;
	for (int y = v1.y; y <= v3.y; y++)
	{
		xLeft += d1;
		xRight += d2;
		DDA_Line(int(xLeft + 0.5), y, int(xRight + 0.5), y, ren);
	}
}



void TriangleFill4(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float d1 = (float)(v2.x - v1.x) / (v2.y - v1.y);
	float d2 = (float)(v3.x - v1.x) / (v3.y - v1.y);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float xLeft = v1.x;
	float xRight = v1.x;
	int y = v1.y;
	for (y; y <= v2.y; y++)
	{
		xLeft += d1;
		xRight += d2;
		DDA_Line(int(xLeft + 0.5), y, int(xRight + 0.5), y, ren);
	}
	d1 = (float)(v3.x - v2.x) / (v3.y - v2.y);
	for (y; y <= v3.y; y++)
	{
		xLeft += d1;
		xRight += d2;
		DDA_Line(int(xLeft + 0.5), y, int(xRight + 0.5), y, ren);
	}
}

void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	ascendingSort(v1, v2, v3);

	if (v1.y == v2.y && v2.y == v3.y)
	{
		TriangleFill1(v1, v2, v3, ren, fillColor);
		return;
	}
	if (v1.y == v2.y && v2.y < v3.y)
	{
		TriangleFill2(v1, v2, v3, ren, fillColor);
		return;
	}
	if (v1.y < v2.y && v2.y == v3.y)
	{
		TriangleFill3(v1, v2, v3, ren, fillColor);
		return;
	}
	else
	{
		TriangleFill4(v1, v2, v3, ren, fillColor);

	}
	return;
}

//======================================================================================================================
//===================================CIRCLE - RECTANGLE - ELLIPSE=======================================================
bool isInsideCircle(int xc, int yc, int R, int x, int y)
{
	if ((x - xc)*(x- xc) + (y - yc)*(y - yc) <= R*R) {
		return 1;
	}
	return 0;
}

void FillIntersection(int x1, int y1, int x2, int y2, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int xmin = minIn3(x1, x2, 1000000);
	int xmax = maxIn3(x1, x2, -100);
	for (int x = xmin; x < xmax; x++) {
		if (isInsideCircle(xc,yc,R,x,y1)==1) {
			SDL_RenderDrawPoint(ren, x, y1);
		}
	}
}

void put4line(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	DDA_Line(xc + x, yc + y, xc - x, yc + y,ren);
	DDA_Line(xc + y, yc + x, xc - y, yc + x, ren);
	DDA_Line(xc + y, yc - x , xc - y, yc - y, ren);
	DDA_Line(xc + x, yc - y, xc - x, yc - y, ren);


}

void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = 0;
	int y = R;
	int p = 1 - R;
	put4line(xc, yc, x, y, ren, fillColor);
	while (x < y)
	{
		if (p < 0)
			p += 2 * x + 3;
		else
		{
			p += 2 * (x - y) + 5;
			y--;
		}
		x++;
		put4line(xc, yc, x, y, ren, fillColor);
	}

}



void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);

	for (int y = vTopLeft.y; y <= vBottomRight.y; y++)
		DDA_Line(vTopLeft.x, y, vBottomRight.x, y, ren);
}




void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);

	for (int y = vTopLeft.y; y <= vBottomRight.y; y++)
		FillIntersection(vTopLeft.x, y, vBottomRight.x, y, xc, yc, R, ren, fillColor);
}


void FillIntersectionEllipseCircle(int xcE, int ycE, int a, int b, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = 0;
	int y = b;
	float p = 2 * ((float)b / a) - (2 * b) + 1;
	FillIntersection(xcE + x, ycE + y, xcE - x, ycE + y, xc, yc, R, ren, fillColor);
	FillIntersection(xcE + x, ycE - y, xcE - x, ycE - y, xc, yc, R, ren, fillColor);

	while ((float)(b / a)*x <= y)
	{
		FillIntersection(xcE + x, ycE + y, xcE - x, ycE + y, xc, yc, R, ren, fillColor);
		FillIntersection(xcE + x, ycE - y, xcE - x, ycE - y, xc, yc, R, ren, fillColor);

		if (p < 0)
		{
			p = p + 2 * ((float)b / a)*(2 * x + 3);
		}
		else 
		{
			p = p - 4 * y + 2 * ((float)b / a)*(2 * x + 3);
			y--;
		}
		x++;
	}

	y = 0;
	x = a;
	p = 2 * ((float)a / b) - 2 * a + 1;
	while (((float)a / b)*y <= x)
	{
		FillIntersection(xcE + x, ycE + y, xcE - x, ycE + y, xc, yc, R, ren, fillColor);
		FillIntersection(xcE + x, ycE - y, xcE - x, ycE - y, xc, yc, R, ren, fillColor);

		if (p < 0)
		{
			p = p + 2 * ((float)a / b)*(2 * y + 3);
		}
		else
		{
			p = p - 4 * x + 2 * ((float)a / b)*(2 * y + 3);
			x = x - 1;
		}
		y = y + 1;
	}
}


void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = 0;
	int y = R1;
	int p = 1 - R1;
	FillIntersection(xc1 + x, yc1 + y, xc1 - x, yc1 + y, xc2, yc2, R2, ren, fillColor);
	FillIntersection(xc1 + y, yc1 + x, xc1 - y, yc1 + x, xc2, yc2, R2, ren, fillColor);
	FillIntersection(xc1 + y, yc1 - x, xc1 - y, yc1 - x, xc2, yc2, R2, ren, fillColor);
	FillIntersection(xc1 + x, yc1 - y, xc1 - x, yc1 - y, xc2, yc2, R2, ren, fillColor);


	while (x < y)
	{
		if (p < 0)
			p += 2 * x + 3;
		else
		{
			p += 2 * (x - y) + 5;
			y--;
		}
		x++;
		FillIntersection(xc1 + x, yc1 + y, xc1 - x, yc1 + y, xc2, yc2, R2, ren, fillColor);
		FillIntersection(xc1 + y, yc1 + x, xc1 - y, yc1 + x, xc2, yc2, R2, ren, fillColor);
		FillIntersection(xc1 + y, yc1 - x, xc1 - y, yc1 - x, xc2, yc2, R2, ren, fillColor);
		FillIntersection(xc1 + x, yc1 - y, xc1 - x, yc1 - y, xc2, yc2, R2, ren, fillColor);
	}
}
