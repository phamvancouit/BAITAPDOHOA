#include "Bezier.h"
#include "Line.h"
#include <iostream>
using namespace std;

///Đưa về vị trí của đường con Bemzier bậc 2 vs các tham số P123 tương ứng vs 3 điểm ( x hoặc y )
double Benzier2(double t, int P1, int P2, int P3) 
{
	double t2 = t * t;
	double mt = 1 - t;
	double mt2 = mt*mt;
	return P1* mt2 + P2*t*mt * 2 + P3*t2;
}
///Đưa về vị trí của đường con Bemzier bậc 3 vs các tham số P1234 tương ứng vs 3 điểm ( x hoặc y )
double Benzier3(double t,int P1, int P2, int P3,int P4) {
	double t2 = t * t;
	double t3 = t2 * t;
	double mt = 1 - t;
	double mt2 = mt*mt;
	double mt3 = mt2 * mt;
	return P1*mt3 + 3*P2*mt2*t + 3*P3*mt*t2 + P4*t3;
}


void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	int arrX[200];
	int arrY[200];
	double t = 0;
	for (int i = 0; i < 200; i ++) {
		t += 0.005;
		arrX[i] = (int)Benzier2(t, p1.x, p2.x, p3.x);
		arrY[i] = (int)Benzier2(t, p1.y, p2.y, p3.y);
	}

	for (int i = 0; i < 199; i ++) {
		SDL_RenderDrawLine(ren, arrX[i], arrY[i], arrX[i + 1], arrY[i + 1]);
	}
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	int arrX[200];
	int arrY[200];
	double t = 0;
	for (int i = 0; i < 200; i++) {
		t += 0.005;
		arrX[i] = (int)Benzier3(t, p1.x, p2.x, p3.x,p4.x);
		arrY[i] = (int)Benzier3(t, p1.y, p2.y, p3.y,p4.y);
	}

	for (int i = 0; i < 199; i++) {
		SDL_RenderDrawLine(ren, arrX[i], arrY[i], arrX[i + 1], arrY[i + 1]);
	}
}


